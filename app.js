const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

// Add headers
// app.use(function (req, res, next) {

//     // Website you wish to allow to connect 
//     res.setHeader('Access-Control-Allow-Origin', '*'); //http://localhost:4200
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'content-type, x-access-token');
//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);

//     // Pass to next layer of middleware
//     next();
// });

app.use(cors());

app.get('/', (req, res, next) => {
    res.end("welcome to root path");
});

app.get('/home', (req, res, next) => {
    res.end("welcome to home path");
});

app.get('/open-app', (req, res, next) => {
    res.end("welcome to API open application.");
});

app.listen(80, () => {
    console.log("Server is running...");
});